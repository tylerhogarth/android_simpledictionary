package za.co.purpleshoe.simpledictionary.thread;


import android.util.Log;

import com.wordnik.client.common.ApiException;

/**
 * Created by Tyer on 4/5/14.
 */
public class DefinitionThread extends Thread {

    private boolean interrupted = false;

    private DefinitionCallBack callBack;
    private String searchWordPhrase;
    private String wordPhrase;

    public DefinitionThread(String wordPhrase) {

        this.wordPhrase = wordPhrase;
        this.searchWordPhrase = wordPhrase.trim().toLowerCase().replaceAll(" ", "%20");
    }

    @Override
    public void run() {

        findWord();
    }

    public void findWord() {

        try {

            //B==D~~~
            DefinitionFactory dFactory = new DefinitionFactory();
            dFactory.buildDefinition(wordPhrase, searchWordPhrase);

            if (callBack != null && !interrupted) {
                callBack.updateUI(dFactory.getdTool());
            }
        } catch (ApiException e) {

            e.printStackTrace();
            if (callBack != null && !interrupted) {
                callBack.loadFailed();
            }
        }
    }

    public void setCallBackListener(DefinitionCallBack cb) {
        this.callBack = cb;
    }

    @Override
    public void interrupt() {
        interrupted = true;
        super.interrupt();
    }

    /**
     * An alternative to interrupt if the thread is not actually running.
     */
    public void cancel() {
        interrupted = true;
    }
}

package za.co.purpleshoe.simpledictionary.thread;

/**
 * Created by Tyler on 2015-03-31.
 */
public class DefinitionTool {

    private String word = "";
    private String other = "";
    private String noun = "";
    private String adjective = "";
    private String verb = "";
    private String adverb = "";
    private String idiom = "";

    public DefinitionTool() {

    }

    public DefinitionTool(String word) {

        this.word = word;
    }

    public DefinitionTool(String word, String other, String noun, String adjective, String verb, String adverb, String idiom) {

        this.word = word;
        this.noun = noun;
        this.adjective = adjective;
        this.verb = verb;
        this.adverb = adverb;
        this.idiom = idiom;
    }

    public void trim() {

        noun = noun.trim();
        adjective = adjective.trim();
        verb = verb.trim();
        adverb = adverb.trim();
        idiom = idiom.trim();
    }

    public void addNoun(String str) {
        noun += str;
    }

    public void addAdjective(String str) {
        adjective += str;
    }

    public void addVerb(String str) {
        verb += str;
    }

    public void addAdverb(String str) {
        adverb += str;
    }

    public void addIdiom(String str) {
        idiom += str;
    }

    public String getNoun() {
        return noun;
    }

    public void setNoun(String noun) {
        this.noun = noun;
    }

    public String getAdjective() {
        return adjective;
    }

    public void setAdjective(String adjective) {
        this.adjective = adjective;
    }

    public String getVerb() {
        return verb;
    }

    public void setVerb(String verb) {
        this.verb = verb;
    }

    public String getAdverb() {
        return adverb;
    }

    public void setAdverb(String adverb) {
        this.adverb = adverb;
    }

    public String getIdiom() {
        return idiom;
    }

    public void setIdiom(String idiom) {
        this.idiom = idiom;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }
}

package za.co.purpleshoe.simpledictionary.thread;

/**
 * Created by Tyler on 2015-03-31.
 */
public interface SearchingCallBack {

    public void cancel();
}

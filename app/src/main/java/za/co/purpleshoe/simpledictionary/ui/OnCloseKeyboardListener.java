package za.co.purpleshoe.simpledictionary.ui;

/**
 * Created by Tyler on 2015-04-06.
 */
public interface OnCloseKeyboardListener {

    public void onCloseKeyboard();
}

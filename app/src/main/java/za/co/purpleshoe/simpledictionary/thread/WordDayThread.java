package za.co.purpleshoe.simpledictionary.thread;

import com.wordnik.client.api.WordsApi;
import com.wordnik.client.common.ApiException;
import com.wordnik.client.model.WordOfTheDay;

import za.co.purpleshoe.simpledictionary.util.Util;

/**
 * Created by Tyler on 2015-03-28.
 */
public class WordDayThread extends Thread {

    private WordDayCallBack callBack;

    @Override
    public void run() {

        WordsApi wordsApi = new WordsApi();

        try {

            wordsApi.addHeader(DefinitionFactory.API_HEADER, DefinitionFactory.API_KEY);

            WordOfTheDay word = wordsApi.getWordOfTheDay(Util.getDate());

            StringBuffer sb = new StringBuffer();

            int size = word.getDefinitions().size();
            for (int i = 0; i < size; i++) {

                sb.append(i + 1);
                sb.append(". ");
                sb.append(word.getDefinitions().get(i).getText());
                if (i + 1 != size)
                    sb.append("\n\n");
            }

            if (callBack != null)
                callBack.wordDaySucceed(Util.capsFirstLetter(word.getWord()), sb.toString());

        } catch (ApiException e) {

            e.printStackTrace();
            if (callBack != null)
                callBack.wordDayFailed();
        }
    }

    public void setListener(WordDayCallBack cb) {
        callBack = cb;
    }
}

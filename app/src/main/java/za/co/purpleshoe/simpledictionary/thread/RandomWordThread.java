package za.co.purpleshoe.simpledictionary.thread;

import android.util.Log;

import com.wordnik.client.api.WordsApi;
import com.wordnik.client.common.ApiException;
import com.wordnik.client.model.WordObject;

/**
 * Created by Tyler on 2015-03-31.
 */
public class RandomWordThread extends Thread {

    private DefinitionCallBack callBack;
    private String word = "";
    private DefinitionThread dThread;

    private boolean interrupted;

    public RandomWordThread() {}

    public RandomWordThread(String word) {
        this.word = word;
    }

    @Override
    public void run() {

        try {

            if (word.isEmpty()) {
                WordsApi wordsApi = new WordsApi();
                wordsApi.addHeader(DefinitionFactory.API_HEADER, DefinitionFactory.API_KEY);
                WordObject wordObj = wordsApi.getRandomWord(
                        null,
                        null,
                        "false",
                        0,
                        0,
                        1,
                        -1,
                        5,
                        -1);
                word = wordObj.getWord();
            }

            if (!interrupted) {
                dThread = new DefinitionThread(word);
                dThread.setCallBackListener(callBack);
                dThread.findWord();
            }

        } catch (ApiException e) {
            if (callBack != null) {
                callBack.loadFailed();
            }
            e.printStackTrace();
        }
    }

    @Override
    public void interrupt() {
        interrupted = true;

        if (dThread != null) {
            dThread.cancel();
        }

        super.interrupt();
    }

    public void setDefinitionListener(DefinitionCallBack cb) {
        callBack = cb;
    }
}

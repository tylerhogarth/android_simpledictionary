package za.co.purpleshoe.simpledictionary.util;

import android.content.Context;
import android.view.Gravity;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;

import za.co.purpleshoe.simpledictionary.R;

/**
 * Created by Tyler on 2015-03-30.
 */
public class Util {

    public static final String WORDNIK_DATE_FORMAT = "yyyy-MM-dd";

    /**
     * Capitalizes the first letter of a string
     * @param word
     */
    public static String capsFirstLetter(String word) {

        if (word.isEmpty())
            return word;

        String first = word.charAt(0) + "";

        return first.toUpperCase() + word.substring(1);
    }

    public static void toggleSoftKeyboard(Context context) {

        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    public static String getDate() {

        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat(WORDNIK_DATE_FORMAT);
        return sdf.format(date);
    }

    public static void loadFailed(Context activity) {

        Toast toast = Toast.makeText(activity, activity.getString(R.string.load_failed), Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.TOP, 0, 100);
        toast.show();

    }
}

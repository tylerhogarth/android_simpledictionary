package za.co.purpleshoe.simpledictionary.util;

/**
 * Created by Tyler on 2015-04-01.
 */
public class Constants {

    //Animation durations
    public static final int WORD_DAY_SLIDE = 500;
    public static final int WORD_DAY_DEFINITION_SLIDE = 250;
    public static final int WORD_DAY_DEFINITION_ALPHA = 100;
    public static final int SEARCH_FAB_SLIDE = 250;
    public static final int SEARCHING_BAR_SLIDE = 250;
    public static final int SEARCHER_BAR_SLIDE = 250;
}

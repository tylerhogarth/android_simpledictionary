package za.co.purpleshoe.simpledictionary.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ScrollView;

import za.co.purpleshoe.simpledictionary.util.Lawg;

/**
 * Created by Tyler on 2015-03-31.
 */
public class ExtendedScrollView extends ScrollView {

    private ScrollChangeListener callBack;
    private int viewBottom = -1;

    public ExtendedScrollView(Context context) {
        super(context);
    }

    public ExtendedScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ExtendedScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public ExtendedScrollView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);

        /* When the user "flings" the scrollview and it reaches the top/bottom
         * the t and oldt go above the max height/below zero. This creates a
         * "bounce" effect. To avoid registering a false up or down scroll
         * greaterZero and hitBottom are used to properly define the top
         * and bottom
         */

        if (viewBottom == -1) {
            View view = getChildAt(getChildCount() - 1);
            viewBottom = view.getBottom();
        }

        int diff = (viewBottom - (getHeight() + getScrollY()));

        //greater than zero means you've reached the top of the scrollview
        boolean greaterZero = t > 0 && oldt > 0;

        //if diff is <= 0 you have reached the bottom
        boolean hitBottom = diff <= 0;

        if (t >= oldt && greaterZero) {
            //scrolling down

            if (callBack != null) {
                callBack.onDownScroll();
            }
        } else if (t < oldt && !hitBottom) {
            //scrolling up

            if (callBack != null) {
                callBack.onUpScroll();
            }
        }
    }

    public void setOnScrollChangeListener(ScrollChangeListener cb) {
        callBack = cb;
    }
}

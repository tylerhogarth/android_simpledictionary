package za.co.purpleshoe.simpledictionary.thread;

/**
 * Created by Tyer on 4/5/14.
 */
public interface DefinitionCallBack {

    public void updateUI(DefinitionTool dTool);

    public void loadFailed();
}

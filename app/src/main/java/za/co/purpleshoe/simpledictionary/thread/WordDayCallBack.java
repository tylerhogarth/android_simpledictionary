package za.co.purpleshoe.simpledictionary.thread;

/**
 * Created by Tyler on 2015-03-30.
 */
public interface WordDayCallBack {

    public void wordDaySucceed(String word, String definition);

    public void wordDayFailed();
}

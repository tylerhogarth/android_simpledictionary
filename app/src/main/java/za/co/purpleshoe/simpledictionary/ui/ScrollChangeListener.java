package za.co.purpleshoe.simpledictionary.ui;

/**
 * Created by Tyler on 2015-03-31.
 */
public interface ScrollChangeListener {

    public void onDownScroll();

    public void onUpScroll();
}

package za.co.purpleshoe.simpledictionary.activity;

import android.animation.Animator;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import za.co.purpleshoe.simpledictionary.R;
import za.co.purpleshoe.simpledictionary.thread.DefinitionCallBack;
import za.co.purpleshoe.simpledictionary.thread.DefinitionTool;
import za.co.purpleshoe.simpledictionary.thread.RandomWordThread;
import za.co.purpleshoe.simpledictionary.thread.SearchingCallBack;
import za.co.purpleshoe.simpledictionary.thread.WordDayCallBack;
import za.co.purpleshoe.simpledictionary.thread.WordDayThread;
import za.co.purpleshoe.simpledictionary.ui.DefinitionView;
import za.co.purpleshoe.simpledictionary.ui.ExtendedEditText;
import za.co.purpleshoe.simpledictionary.ui.ExtendedScrollView;
import za.co.purpleshoe.simpledictionary.ui.OnCloseKeyboardListener;
import za.co.purpleshoe.simpledictionary.ui.ScrollChangeListener;
import za.co.purpleshoe.simpledictionary.util.Constants;
import za.co.purpleshoe.simpledictionary.util.UserPreferences;
import za.co.purpleshoe.simpledictionary.util.Util;

/**
 * Created by Tyer on 4/5/14.
 */
public class LandingActivity extends Activity implements ScrollChangeListener, DefinitionCallBack, WordDayCallBack, SearchingCallBack, View.OnClickListener {

    private LinearLayout ultraParent;
    private ExtendedScrollView scrollView;
    private LinearLayout mainLayout;
    private ImageView sdLogo;
    private ImageView searchFab;
    private TextView wordDayTv;
    private TextView wordDayDefTv;
    private ProgressBar wordDayPb;
    private ImageView wordDaySearchIv;
    private LinearLayout wordDayUltraParent;
    private RelativeLayout wordDayParent;
    private LinearLayout searchingLayout;
    private TextView searchingWordTv;
    private LinearLayout searcherParent;
    private ExtendedEditText inputEt;
    private View focusStealer;

    private DefinitionView definitionView;

    private RandomWordThread randomWordThread;

    /**
     * The bottom y position of the word of the day text view
     */
    private int wordDayBottom = 0;

    /**
     * The amount of space the search FAB needs to animate downward
     */
    private int fabSize = 250;

    private boolean searchFabVisible = true;
    private boolean searching = false;
    private boolean wordDayExpanded = false;
    private boolean definitionsExpanded = false;

    //true if this is the first word the user has loocked up
    private boolean firstWord = false;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.status_bar));
        }

        ultraParent = findViewById(R.id.activity_landing_ultra_parent);
        scrollView = findViewById(R.id.activity_landing_scroller);
        mainLayout = findViewById(R.id.activity_landing_def_layout);
        sdLogo = findViewById(R.id.activity_landing_sd_logo_iv);
        LinearLayout definitionViewLayout = findViewById(R.id.activity_landing_definition_view_layout);
        searchFab = findViewById(R.id.activity_landing_search_fab);
        searchFab.setOnClickListener(this);
        wordDayTv = findViewById(R.id.activity_landing_word_day_tv);
        wordDayDefTv = findViewById(R.id.activity_landing_word_day_def_tv);
        wordDayPb = findViewById(R.id.activity_landing_word_day_pb);
        wordDaySearchIv = findViewById(R.id.activity_landing_word_day_search_iv);
        wordDaySearchIv.setOnClickListener(this);
        wordDayUltraParent = findViewById(R.id.activity_landing_word_day_ultra_parent);
        wordDayParent = findViewById(R.id.activity_landing_word_day_parent);
        searchingLayout = findViewById(R.id.view_searching_searching_layout);
        searchingWordTv = findViewById(R.id.view_searching_searching_for_word);
        searcherParent = findViewById(R.id.view_searcher_parent);
        inputEt = findViewById(R.id.view_searcher_input_et);
        focusStealer = findViewById(R.id.view_searcher_focus_stealer_v);
        findViewById(R.id.view_searching_cancel_iv).setOnClickListener(this);

        ultraParent.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    ultraParent.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                } else {
                    ultraParent.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                }

                //calculates fabSize by getting the frame size and subtract the top of the fab so that it
                //animates down only by its actual height.
                fabSize = findViewById(R.id.activity_landing_frame_ultra_parent).getBottom() - searchFab.getTop();

                pullDownWordDay();
            }
        });

        scrollView.setOnScrollChangeListener(this);

        final Context c = this;
        inputEt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    Util.toggleSoftKeyboard(c);
                    searchWord(inputEt.getText().toString());
                    return true;
                }
                return false;
            }
        });

        inputEt.setOnCloseKeyboardListener(new OnCloseKeyboardListener() {
            @Override
            public void onCloseKeyboard() {

                hideSearcher(true);
            }
        });

        definitionView = new DefinitionView(this);

        String wrd = UserPreferences.getString(this, UserPreferences.WORD, null);
        definitionViewLayout.addView(definitionView.inflateView(wrd));
        if (wrd == null) {

            firstWord = true;
            mainLayout.setVisibility(View.GONE);
            sdLogo.setVisibility(View.VISIBLE);
        }

        String date = UserPreferences.getString(this, UserPreferences.WORD_DAY_DATE, "");
        if (date.equals(Util.getDate())) {

            wordDayTv.setText(UserPreferences.getString(this, UserPreferences.WORD_DAY, getString(R.string.word_day)));
            wordDayDefTv.setText(UserPreferences.getString(this, UserPreferences.WORD_DAY_DEFINITION, ""));
            wordDaySearchIv.setVisibility(View.VISIBLE);
        } else {

            wordDayPb.setVisibility(View.VISIBLE);

            WordDayThread wordDayThread = new WordDayThread();
            wordDayThread.setListener(this);
            wordDayThread.start();
        }
    }

    /**
     * Animates the definitions and word of the day downward onto the screen.
     */
    public void pullDownWordDay() {

        if (wordDayExpanded) {
            return;
        }

        wordDayBottom = wordDayParent.getBottom();

        searchingLayout.animate().translationY(-wordDayBottom).setDuration(0);
        searchingLayout.setVisibility(View.VISIBLE);
        searcherParent.animate().translationY(-wordDayBottom).setDuration(0);
        searcherParent.setVisibility(View.VISIBLE);

        wordDayUltraParent.animate().translationY(-wordDayBottom).setDuration(0).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {}

            @Override
            public void onAnimationEnd(Animator animation) {

                wordDayUltraParent.setVisibility(View.VISIBLE);
                wordDayUltraParent.animate().translationY(0).setDuration(Constants.WORD_DAY_SLIDE);
            }

            @Override
            public void onAnimationCancel(Animator animation) {}

            @Override
            public void onAnimationRepeat(Animator animation) {}
        });

        ultraParent.animate().translationY(wordDayBottom).setDuration(Constants.WORD_DAY_SLIDE);
        scrollView.setPadding(0, 0, 0, wordDayBottom);

        wordDayExpanded = true;
    }

    /**
     * Sets the word of the day definitions to visible and animates the definitions
     * downward so that they have space to be seen.
     */
    public void expandWordDayDefinitions() {

        wordDaySearchIv.animate().rotation(180).setDuration(Constants.WORD_DAY_DEFINITION_SLIDE);
        wordDayDefTv.setVisibility(View.INVISIBLE);
        wordDayDefTv.animate().alpha(0f).setDuration(100).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {}

            @Override
            public void onAnimationEnd(Animator animation) {
                ultraParent.animate().translationY(wordDayUltraParent.getHeight())
                        .setDuration(Constants.WORD_DAY_DEFINITION_SLIDE)
                        .setListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animation) {
                            }

                            @Override
                            public void onAnimationEnd(Animator animation) {
                                scrollView.setPadding(0, 0, 0, wordDayUltraParent.getHeight());
                                wordDayDefTv.setVisibility(View.VISIBLE);
                                wordDayDefTv.animate().alpha(1f)
                                        .setDuration(Constants.WORD_DAY_DEFINITION_ALPHA)
                                        .setListener(new Animator.AnimatorListener() {
                                            @Override
                                            public void onAnimationStart(Animator animation) {
                                            }

                                            @Override
                                            public void onAnimationEnd(Animator animation) {
                                                //must clear the animations so that we can reverse this animation
                                                wordDayDefTv.clearAnimation();
                                                definitionsExpanded = true;
                                            }

                                            @Override
                                            public void onAnimationCancel(Animator animation) {
                                            }

                                            @Override
                                            public void onAnimationRepeat(Animator animation) {
                                            }
                                        });
                            }

                            @Override
                            public void onAnimationCancel(Animator animation) {
                            }

                            @Override
                            public void onAnimationRepeat(Animator animation) {
                            }
                        });

            }

            @Override
            public void onAnimationCancel(Animator animation) {}

            @Override
            public void onAnimationRepeat(Animator animation) {}
        });
    }

    /**
     * Does the opposite of expandWordOfDayDefinitions
     */
    public void collapseWordDayDefinitions() {

        wordDaySearchIv.animate().rotation(0).setDuration(Constants.WORD_DAY_DEFINITION_SLIDE);
        wordDayDefTv.animate().alpha(0f)
                .setDuration(Constants.WORD_DAY_DEFINITION_ALPHA)
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {}

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        wordDayDefTv.clearAnimation();
                        wordDayDefTv.setVisibility(View.INVISIBLE);
                        ultraParent.animate().translationY(wordDayBottom).setDuration(Constants.WORD_DAY_DEFINITION_SLIDE).setListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animation) {}

                            @Override
                            public void onAnimationEnd(Animator animation) {
                                scrollView.setPadding(0, 0, 0, wordDayBottom);
                                definitionsExpanded = false;
                            }

                            @Override
                            public void onAnimationCancel(Animator animation) {}

                            @Override
                            public void onAnimationRepeat(Animator animation) {}
                        });
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {}

                    @Override
                    public void onAnimationRepeat(Animator animation) {}
                });
    }

    public void searchWord(String word) {

        searching = true;
        if (randomWordThread != null) {
            randomWordThread.interrupt();
        }

        randomWordThread = new RandomWordThread(word);
        randomWordThread.setDefinitionListener(this);
        randomWordThread.start();

        if (word.isEmpty()) {
            searchingWordTv.setText(getString(R.string.random_word));

            if (searchFabVisible) {

                hideSearchFab();
            } else {

                hideSearcher(false);
                Util.toggleSoftKeyboard(this);
            }
        } else {
            searchingWordTv.setText(word);

            hideSearcher(false);
        }
        showSearchingLayout();
    }

    void searchFabClick() {

        showSearcher();
        hideSearchFab();
    }

    @Override
    public void onDownScroll() {

        hideSearchFab();
    }

    @Override
    public void onUpScroll() {

        showSearchFab();
    }

    @Override
    public void updateUI(final DefinitionTool dTool) {

        searching = false;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                definitionView.resetDefinition();
                definitionView.setVariables(dTool.getWord(),
                        dTool.getOther(),
                        dTool.getNoun(),
                        dTool.getAdjective(),
                        dTool.getVerb(),
                        dTool.getAdverb(),
                        dTool.getIdiom());

                hideSearchingLayout();
                definitionView.storeWord();

                if (firstWord) {

                    sdLogo.setVisibility(View.GONE);
                    mainLayout.setVisibility(View.VISIBLE);
                }

                if (!searchFabVisible) {
                    showSearchFab();
                }
            }
        });
    }

    @Override
    public void loadFailed() {

        searching = false;
        final Context c = this;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                Util.loadFailed(c);

                if (!searchFabVisible) {
                    showSearchFab();
                }

                hideSearchingLayout();
            }
        });
    }

    @Override
    public void wordDaySucceed(final String word, final String definition) {

        final Context context = this;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                wordDayPb.setVisibility(View.GONE);
                wordDaySearchIv.setVisibility(View.VISIBLE);
                wordDayTv.setText(word);
                wordDayDefTv.setText(definition);

                UserPreferences.setString(context, UserPreferences.WORD_DAY_DATE, Util.getDate());
                UserPreferences.setString(context, UserPreferences.WORD_DAY, word);
                UserPreferences.setString(context, UserPreferences.WORD_DAY_DEFINITION, definition);
            }
        });
    }

    @Override
    public void wordDayFailed() {

        final Context c = this;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                wordDayPb.setVisibility(View.GONE);

                Util.loadFailed(c);
            }
        });
    }

    void wordDaySearchClick() {

        if (!definitionsExpanded) {
            expandWordDayDefinitions();
        } else {
            collapseWordDayDefinitions();
        }
    }

    @Override
    public void cancel() {

        if (randomWordThread != null) {
            randomWordThread.interrupt();
        }
    }

    void cancelSearchClick() {

        searching = false;
        if (randomWordThread != null) {
            randomWordThread.interrupt();
        }

        hideSearchingLayout();

        if (!searchFabVisible) {
            showSearchFab();
        }
    }

    public void showSearchingLayout() {

        searchingLayout.animate().translationY(0).setDuration(Constants.SEARCHING_BAR_SLIDE);

        if (definitionsExpanded) {
            collapseWordDayDefinitions();
        }
    }

    public void hideSearchingLayout() {

        searchingLayout.animate().translationY(-wordDayBottom).setDuration(Constants.SEARCHING_BAR_SLIDE);
    }

    public void showSearcher() {

        searcherParent.animate().translationY(0).setDuration(Constants.SEARCHER_BAR_SLIDE);
        Util.toggleSoftKeyboard(this);
        inputEt.requestFocus();
        inputEt.setKeyboardOpen(true);
        hideSearchFab();

        if (definitionsExpanded) {
            collapseWordDayDefinitions();
        }
    }

    public void hideSearcher(boolean hideFab) {

        searcherParent.animate().translationY(-wordDayBottom).setDuration(Constants.SEARCHER_BAR_SLIDE);
        //Util.closeSoftKeyboard(this);
        inputEt.setText("");
        inputEt.setKeyboardOpen(false);
        focusStealer.requestFocus();
        if (hideFab)
            showSearchFab();
    }

    public void showSearchFab() {

        if (!searchFabVisible) {
            searchFab.animate().translationY(0).setDuration(Constants.SEARCH_FAB_SLIDE);
            searchFabVisible = true;
        }
    }

    public void hideSearchFab() {

        if (searchFabVisible) {

            searchFab.animate().translationY(fabSize).setDuration(Constants.SEARCH_FAB_SLIDE);
            searchFabVisible = false;
        }
    }

    @Override
    public void onBackPressed() {

        //only one of these can be true at a time
        if (searching) {
            cancelSearchClick();
        } else if (definitionsExpanded) {
            collapseWordDayDefinitions();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.activity_landing_search_fab:
                searchFabClick();
                break;

            case R.id.view_searching_cancel_iv:
                cancelSearchClick();
                break;

            case R.id.activity_landing_word_day_search_iv:
                wordDaySearchClick();
                break;
        }
    }
}
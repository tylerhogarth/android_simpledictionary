package za.co.purpleshoe.simpledictionary.ui;

import android.content.Context;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import za.co.purpleshoe.simpledictionary.R;
import za.co.purpleshoe.simpledictionary.util.UserPreferences;
import za.co.purpleshoe.simpledictionary.util.Util;

/**
 * Created by Tyler on 2015-03-30.
 */
public class DefinitionView {

    private Context context;
    private View view;

    private String word = "";
    private String other;
    private String noun;
    private String adjective;
    private String verb;
    private String adverb;
    private String idiom;

    private TextView wordTv;
    private TextView noDefTv;
    private TextView nounTv;
    private TextView nounCtTv;
    private TextView adjectiveTv;
    private TextView adjectiveCtTv;
    private TextView verbTv;
    private TextView verbCtTv;
    private TextView adverbTv;
    private TextView adverbCtTv;
    private TextView idiomTv;
    private TextView idiomCtTv;
    
    public DefinitionView(Context context) {
        
        this.context = context;
    }
    
    public void setVariables(String word,
                             String other,
                             String noun,
                             String adjective,
                             String verb,
                             String adverb,
                             String idiom) {
        
        this.word = word;
        this.other = other;
        this.noun = noun;
        this.adjective = adjective;
        this.verb = verb;
        this.adverb = adverb;
        this.idiom = idiom;

        setUI();
    }

    public void setUI() {

        if (view != null) {
            if (!noun.isEmpty()) {
                nounCtTv.setText(parseDefinitionString(noun));
                nounTv.setVisibility(View.VISIBLE);
                nounCtTv.setVisibility(View.VISIBLE);
            } else {
                nounTv.setVisibility(View.GONE);
                nounCtTv.setVisibility(View.GONE);
            }
            if (!adjective.isEmpty()) {
                adjectiveCtTv.setText(parseDefinitionString(adjective));
                adjectiveTv.setVisibility(View.VISIBLE);
                adjectiveCtTv.setVisibility(View.VISIBLE);
            } else {
                adjectiveTv.setVisibility(View.GONE);
                adjectiveCtTv.setVisibility(View.GONE);
            }
            if (!verb.isEmpty()) {
                verbCtTv.setText(parseDefinitionString(verb));
                verbTv.setVisibility(View.VISIBLE);
                verbCtTv.setVisibility(View.VISIBLE);
            } else {
                verbTv.setVisibility(View.GONE);
                verbCtTv.setVisibility(View.GONE);
            }
            if (!adverb.isEmpty()) {
                adverbCtTv.setText(parseDefinitionString(adverb));
                adverbTv.setVisibility(View.VISIBLE);
                adverbCtTv.setVisibility(View.VISIBLE);
            } else {
                adverbTv.setVisibility(View.GONE);
                adverbCtTv.setVisibility(View.GONE);
            }
            if (!idiom.isEmpty()) {
                idiomCtTv.setText(parseDefinitionString(idiom));
                idiomTv.setVisibility(View.VISIBLE);
                idiomCtTv.setVisibility(View.VISIBLE);
            } else {
                idiomTv.setVisibility(View.GONE);
                idiomCtTv.setVisibility(View.GONE);
            }

            if (noun.isEmpty() && adjective.isEmpty() && verb.isEmpty() && adverb.isEmpty() && idiom.isEmpty()) {

                if (other.isEmpty()) {
                    noDefTv.setText(context.getString(R.string.no_definitions));
                } else {
                    noDefTv.setText(parseDefinitionString(other));
                }
                noDefTv.setVisibility(View.VISIBLE);
            } else {
                noDefTv.setVisibility(View.GONE);
            }

            wordTv.setText(Util.capsFirstLetter(word));
        }
    }

    /**
     * The API returns text in HTML form. It also sometimes returns 'null' inside the string
     * where definitions should be.
     *
     * Because of this, we format the string, remove the numbering and null strings and implement
     * our own numbering.
     *
     * I was lazy when I made this so just chill on the judgement. I wasn't planning on winning
     * any development awards with this.
     *
     * @param definition
     * @return
     */
    private String parseDefinitionString(String definition) {

        String regex = "!@#-#@!";
        definition = definition.replaceAll("\n", regex);

        String[] args = definition.split(regex);
        StringBuilder builder = new StringBuilder();
        int i = 0;
        for (String str : args) {
            if (str.length() == 0) {
                 continue;
            }
            try {
                if (str.contains(".")) {
                    str = str.substring(str.indexOf('.') + 2);
                }
                if (str.startsWith("null")) {
                    continue;
                }
                i++;
                if (i != 1) {
                    builder.append("<br>");
                    builder.append("<br>");
                }
                builder.append(i);
                builder.append(". ");
                builder.append(str);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return Html.fromHtml(builder.toString()).toString();
    }

    public View inflateView(String wrd) {

        view = View.inflate(context, R.layout.view_definition, null);

        wordTv = (TextView) view.findViewById(R.id.view_definition_word_hd);
        noDefTv = (TextView) view.findViewById(R.id.view_definition_none_tv);
        nounTv = (TextView) view.findViewById(R.id.view_definition_noun_hd);
        nounCtTv = (TextView) view.findViewById(R.id.view_definition_noun_ct);
        adjectiveTv = (TextView) view.findViewById(R.id.view_definition_adjective_hd);
        adjectiveCtTv = (TextView) view.findViewById(R.id.view_definition_adjective_ct);
        verbTv = (TextView) view.findViewById(R.id.view_definition_verb_hd);
        verbCtTv = (TextView) view.findViewById(R.id.view_definition_verb_ct);
        adverbTv = (TextView) view.findViewById(R.id.view_definition_adverb_hd);
        adverbCtTv = (TextView) view.findViewById(R.id.view_definition_adverb_ct);
        idiomTv = (TextView) view.findViewById(R.id.view_definition_idiom_hd);
        idiomCtTv = (TextView) view.findViewById(R.id.view_definition_idiom_ct);

        if (wrd != null) {

            word = wrd;
            other = UserPreferences.getString(context, UserPreferences.OTHER, "");
            noun = UserPreferences.getString(context, UserPreferences.NOUN, "");
            adjective = UserPreferences.getString(context, UserPreferences.ADJECTIVE, "");
            verb = UserPreferences.getString(context, UserPreferences.VERB, "");
            adverb = UserPreferences.getString(context, UserPreferences.ADVERB, "");
            idiom = UserPreferences.getString(context, UserPreferences.IDIOM, "");

            setUI();
        }

        return view;
    }

    public void resetDefinition() {
        nounCtTv.setText("");
        adjectiveCtTv.setText("");
        verbCtTv.setText("");
        adverbCtTv.setText("");
        idiomCtTv.setText("");
        wordTv.setText("");
    }
    
    public void storeWord() {

        UserPreferences.setString(context, UserPreferences.WORD, word);
        UserPreferences.setString(context, UserPreferences.OTHER, other);
        UserPreferences.setString(context, UserPreferences.NOUN, noun);
        UserPreferences.setString(context, UserPreferences.ADJECTIVE, adjective);
        UserPreferences.setString(context, UserPreferences.VERB, verb);
        UserPreferences.setString(context, UserPreferences.ADVERB, adverb);
        UserPreferences.setString(context, UserPreferences.IDIOM, idiom);
    }
}

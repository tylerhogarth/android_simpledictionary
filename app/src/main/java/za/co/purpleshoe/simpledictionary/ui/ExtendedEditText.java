package za.co.purpleshoe.simpledictionary.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.widget.EditText;

import za.co.purpleshoe.simpledictionary.util.Lawg;

/**
 * Created by Tyler on 2015-04-06.
 */
public class ExtendedEditText extends EditText {

    private boolean keyboardOpen = false;
    private OnCloseKeyboardListener callBack;

    public ExtendedEditText(Context context) {
        super(context);
    }

    public ExtendedEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ExtendedEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public ExtendedEditText(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    public boolean onKeyPreIme(int keyCode, KeyEvent event) {

        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            if (callBack != null && keyboardOpen) {
                callBack.onCloseKeyboard();
            }
        }
        return super.onKeyPreIme(keyCode, event);
    }

    public void setOnCloseKeyboardListener(OnCloseKeyboardListener cb) {
        callBack = cb;
    }

    public boolean isKeyboardOpen() {
        return keyboardOpen;
    }

    public void setKeyboardOpen(boolean keyboardOpen) {
        this.keyboardOpen = keyboardOpen;
    }
}

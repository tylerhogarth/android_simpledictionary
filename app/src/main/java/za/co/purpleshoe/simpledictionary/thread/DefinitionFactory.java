package za.co.purpleshoe.simpledictionary.thread;

import com.wordnik.client.api.WordApi;
import com.wordnik.client.common.ApiException;
import com.wordnik.client.model.Definition;

import java.util.List;

/**
 * Created by Tyler on 2015-03-31.
 */
public class DefinitionFactory {

    public static final String API_KEY = "aefd3962bbf837ec70105059c9701c6c68bd3b7a6364d6f4c";
    public static final String API_HEADER = "api_key";

    public static final String POS_NOUN = "noun";
    public static final String POS_NOUN_PLURAL = "noun-plural";
    public static final String POS_NOUN_POSESSIVE = "noun-posessive";
    public static final String POS_ADJECTIVE = "adjective";
    public static final String POS_VERB = "verb";
    public static final String POS_VERB_INT = "verb-intransitive";
    public static final String POS_VERB_TRA = "verb-transitive";
    public static final String POS_ADVERB = "adverb";
    public static final String POS_IDIOM = "idiom";

    private DefinitionTool dTool = new DefinitionTool();

    private int otherPosition = 1;
    private int nounPosition = 1;
    private int adjectivePosition = 1;
    private int verbPosition = 1;
    private int adverbPosition = 1;
    private int idiomPosition = 1;

    public void buildDefinition(List<Definition> definitions) {

    }

    public void buildDefinition(String word, String searchWord) throws ApiException {

        dTool.setWord(word);

        WordApi wordApi = new WordApi();

        wordApi.addHeader(API_HEADER, API_KEY);

        List<Definition> definitions = wordApi.getDefinitions(
                searchWord,
                null,
                null,
                500,
                "false",
                "false",
                "false");

        sortDefinitions(definitions);
    }

    private void sortDefinitions(List<Definition> definitions) {

        StringBuilder other = new StringBuilder();
        StringBuilder noun = new StringBuilder();
        StringBuilder adjective = new StringBuilder();
        StringBuilder verb = new StringBuilder();
        StringBuilder adverb = new StringBuilder();
        StringBuilder idiom = new StringBuilder();

        if (definitions != null) {

            for (Definition definition : definitions) {

                String pos = definition.getPartOfSpeech();

                if (pos == null) {
                    other.append(otherPosition++ + ". " + definition.getText() + "\n\n");
                } else if (pos.equals(DefinitionFactory.POS_NOUN) || pos.equals(DefinitionFactory.POS_NOUN_PLURAL) || pos.equals(DefinitionFactory.POS_NOUN_POSESSIVE)) {
                    noun.append(nounPosition++ + ". " + definition.getText() + "\n\n");
                } else if (pos.equals(DefinitionFactory.POS_ADJECTIVE)) {
                    adjective.append(adjectivePosition++ + ". " + definition.getText() + "\n\n");
                } else if (pos.equals(DefinitionFactory.POS_VERB) || pos.equals(DefinitionFactory.POS_VERB_INT) || pos.equals(DefinitionFactory.POS_VERB_TRA)) {
                    verb.append(verbPosition++ + ". " + definition.getText() + "\n\n");
                } else if (pos.equals(DefinitionFactory.POS_ADVERB)) {
                    adverb.append(adverbPosition++ + ". " + definition.getText() + "\n\n");
                } else if (pos.equals(DefinitionFactory.POS_IDIOM)) {
                    idiom.append(idiomPosition++ + ". " + definition.getText() + "\n\n");
                }
            }

            dTool.setOther(other.toString().trim());
            dTool.setNoun(noun.toString().trim());
            dTool.setAdjective(adjective.toString().trim());
            dTool.setVerb(verb.toString().trim());
            dTool.setAdverb(adverb.toString().trim());
            dTool.setIdiom(idiom.toString().trim());
        }
    }

    private String convertDefinitionToString(List<Definition> defList, String pos) {

        StringBuilder sb = new StringBuilder();
        if (pos.equals(DefinitionFactory.POS_NOUN) || pos.equals(DefinitionFactory.POS_NOUN_PLURAL) || pos.equals(DefinitionFactory.POS_NOUN_POSESSIVE))
            for (Definition def : defList)
                sb.append(nounPosition++ + ". " + def.getText() + "\n\n");
        if (pos.equals(DefinitionFactory.POS_ADJECTIVE))
            for (Definition def : defList)
                sb.append(adjectivePosition++ + ". " + def.getText() + "\n\n");
        if (pos.equals(DefinitionFactory.POS_VERB) || pos.equals(DefinitionFactory.POS_VERB_INT) || pos.equals(DefinitionFactory.POS_VERB_TRA))
            for (Definition def : defList)
                sb.append(verbPosition++ + ". " + def.getText() + "\n\n");
        if (pos.equals(DefinitionFactory.POS_ADVERB))
            for (Definition def : defList)
                sb.append(adverbPosition++ + ". " + def.getText() + "\n\n");
        if (pos.equals(DefinitionFactory.POS_IDIOM))
            for (Definition def : defList)
                sb.append(idiomPosition++ + ". " + def.getText() + "\n\n");

        return sb.toString();
    }

    public DefinitionTool getdTool() {
        return dTool;
    }

    public void setdTool(DefinitionTool dTool) {
        this.dTool = dTool;
    }
}
